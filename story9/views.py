from django.shortcuts import render, redirect 
from django.http import HttpResponse
# Create your views here.
from .models import Book
import json
from django.http import JsonResponse
from django.core import serializers
def story9(request):
    return render(request,"story9.html")

def likebook(request):
    if request.method == 'GET':
        image = request.GET['image']
        title = request.GET['title']
        author = request.GET['author']
        penerbit = request.GET['penerbit']
        tahun = request.GET['tahun']

    try:
        data = Book.objects.get(title=title)
        data.like+=1
        data.save()
    except:
        Book.objects.create(
            image = image,
            title = title,
            author = author,
            penerbit = penerbit,
            tahun = tahun,
            like = 1
        )
    return HttpResponse('story9')

def gettop5(request):
    # data = serializers.serialize("xml", Book.objects.order_by('-like'))
    # print(1)
    # return HttpResponse(data[:5], content_type='application/json')
    temp = Book.objects.order_by('-like').values()  # or simply .values() to get all fields
    data = list(temp)
    return JsonResponse(data[:5], safe=False)