from django.urls import path
from .views import story9, likebook, gettop5
 
urlpatterns = [
	path('story9/', story9, name = 'story9'),
	path('story9/likebook/',likebook,name='likebook'),
	path('story9/gettop/',gettop5,name='gettop5'),
]
