from django.test import TestCase, Client, LiveServerTestCase
from django.urls import reverse, resolve
from .views import story9, likebook, gettop5
from .models import Book
from .apps import Story9Config
# from .forms import Formpesan
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options

class story9Test(TestCase):
    def test_story9_resolv(self):
        url = reverse('story9')
        self.assertEquals(resolve(url).func,story9)


    def test_story9_exists(self):
        response = self.client.get(reverse('story9'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,'story9.html')

    def test_apps(self):
        self.assertEqual(Story9Config.name, 'story9')

    def test_book_model(self):
        model = Book.objects.create(image='inigambar',title='aaaa',author='bbbb',penerbit='cccc',tahun='1111',like=1)
        self.assertEqual(Book.objects.all().count(),1)
        self.assertEquals(str(Book.objects.get(pk=1)),'aaaa')

    def test_like_url(self):
        url = reverse('likebook')
        self.assertEquals(resolve(url).func,likebook)

    def test_top5_url(self):
        url = reverse('gettop5')
        self.assertEquals(resolve(url).func,gettop5) 

    def test_view_like(self):
        model = Book.objects.create(image='inigambar',title='aaaa',author='bbbb',penerbit='cccc',tahun='1111',like=1)
        response_post = self.client.get(reverse('likebook'), {'image':'inigambar','title':'aaaa','author':'bbbb','penerbit':'cccc','tahun':'1111'})
        self.assertEqual(response_post.status_code, 200)