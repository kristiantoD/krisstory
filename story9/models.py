from django.db import models

# Create your models here.
class Book(models.Model):
	image =  models.CharField(max_length=200)
	title = models.CharField(max_length=100)
	author  = models.CharField(max_length=50)
	penerbit = models.CharField(max_length=50)
	tahun = models.CharField(max_length=50)
	like = models.IntegerField()
	def __str__(self):
		return self.title