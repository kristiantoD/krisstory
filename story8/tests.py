from django.test import TestCase, Client, LiveServerTestCase
from django.urls import reverse, resolve
from .views import story8
from .models import Data
from .apps import Story8Config
# from .forms import Formpesan
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options

class story8Test(TestCase):
    def test_story8_resolv(self):
        url = reverse('story8')
        self.assertEquals(resolve(url).func,story8)


    def test_story8_exists(self):
        response = self.client.get(reverse('story8'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,'story8.html')

    def test_model_data(self):
        model = Data.objects.create(head='aaaa',pesan='bbbb')
        self.assertEqual(Data.objects.all().count(),1)
        self.assertEquals(str(Data.objects.get(pk=1)),'aaaa')

    def test_apps(self):
        self.assertEqual(Story8Config.name, 'story8')