from django.shortcuts import render, redirect 
from django.http import HttpResponse
# Create your views here.
from .models import Data

def story8(request):
	context={
		"data": Data.objects.all()
	}
	return render(request,"story8.html",context)