from django.test import TestCase, Client, LiveServerTestCase
from django.urls import reverse, resolve
from .views import register, loginred, logoutred
from .models import Profile
from .apps import Story10Config
# from .forms import Formpesan
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options
from django.utils import timezone

class story10Test(TestCase):
    def test_apps(self):
        self.assertEqual(Story10Config.name, 'story10')

    def test_story10_signup_resolv(self):
        url = reverse('register')
        self.assertEquals(resolve(url).func,register)


    def test_story10_signup_exists(self):
        response = self.client.get(reverse('register'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,'signup.html')

    def test_signup(self):
    	response_post = self.client.post(reverse('register'), {'username':'pamankupergikedesa','password1':'kupu-kupumakan123','password2':'kupu-kupumakan123'})
    	self.assertEqual(response_post.status_code, 302)

    def test_story10_loginred_resolv(self):
        url = reverse('loginred')
        self.assertEquals(resolve(url).func,loginred)

    def test_story10_loginred_exists(self):
        response = self.client.get(reverse('loginred'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,'loginred.html')

    def test_story10_logout_resolv(self):
        url = reverse('logoutred')
        self.assertEquals(resolve(url).func,logoutred)

    def test_story10_logoutred_exists(self):
        response = self.client.get(reverse('logoutred'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,'logoutred.html')

    def test_profile_model(self):
        model = Profile.objects.create(image='inigambar',depan='aaaa',belakang='bbbb',tanggal=timezone.now())
        self.assertEqual(Profile.objects.all().count(),1)
        self.assertEquals(str(Profile.objects.get(pk=1)),'aaaa')
