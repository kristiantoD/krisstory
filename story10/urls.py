from django.urls import path
from .views import register, loginred, logoutred, profile
 
urlpatterns = [
	path('signup/', register, name = 'register'),
	path('loginred/', loginred, name = 'loginred'),
	path('logoutred/', logoutred, name = 'logoutred'),
	path('profile/', profile, name = 'profile'),


]
