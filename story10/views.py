from django.shortcuts import render, redirect 
from django.http import HttpResponse
# Create your views here.
from .forms import RegisForm
from .models import Profile
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login, authenticate

def register(response):
    if response.method == "POST":
        form = UserCreationForm(response.POST)
        if form.is_valid():
            form.save()
            return redirect("story9")

    else:
        form = UserCreationForm()
        

    return render(response, "signup.html", {"form":form})


def loginred(request):
	current = request.user
	username = current.username
	return render(request, "loginred.html", {"username":username})

def logoutred(request):
	return render(request, "logoutred.html")

def profile(request):
    current = request.user
    profile = Profile.objects.get(user = current)
    return render(request, "profile.html",{"profile":profile})