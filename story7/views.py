from django.shortcuts import render, redirect 
from django.http import HttpResponse
# Create your views here.
from .forms import Formpesan
from .models import Pesan, Pesantemp

def index(request):
	context={
		"data": Pesan.objects.all()
	}
	return render(request,"index.html",context)

def tambah(request):
	context = {
		"form":Formpesan
	}
	return render(request,"tambahpesan.html",context)

def cektambah(request):
	form = Formpesan(request.POST)
	if request.method == 'POST':
		pesan = request.POST['pesan']
		nama = request.POST['nama']

	Pesantemp.objects.create(nama=nama, pesan=pesan)
	context = {
		"pesan":pesan,
		"nama":nama,
	}
	return render(request,"cekpesan.html",context)

def validasi(request):
	data = Pesantemp.objects.last()
	if 'tidak' in request.POST:
		Pesantemp.objects.all().delete()
		return redirect('tambah')
	elif 'ya' in request.POST:
		Pesan.objects.create(nama=data.nama, pesan=data.pesan, warna="blue", count=1)
		Pesantemp.objects.all().delete()
		return redirect('index')

def warna(request, pk):
	data = Pesan.objects.get(pk=pk)
	data.count+=1
	if data.count%4==0:
		data.warna="red"
	elif data.count%4==1:
		data.warna="blue"
	elif data.count%4==2:
		data.warna="green"	
	else:
		data.warna="yellow"
	data.save()
	return redirect('index')