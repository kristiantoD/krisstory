from django.contrib import admin

# Register your models here.
from .models import Pesan, Pesantemp


admin.site.register(Pesan)
admin.site.register(Pesantemp)