from django.urls import path
from .views import index, tambah, cektambah, validasi, warna
 
urlpatterns = [
	path('', index, name = 'index'),
	path('tambah/', tambah, name = 'tambah'),
	path('cektambah/', cektambah, name = 'cektambah'),
	path('validasi/', validasi, name = 'validasi'),
	path('warna/<int:pk>', warna, name = 'warna'),


]
