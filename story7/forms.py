from django import forms 

class Formpesan(forms.Form):
	nama = forms.CharField(max_length=50,
							widget=forms.TextInput(attrs={
								'class':'form-control',
								'placeholder':'nama'
								}))

	pesan = forms.CharField(widget=forms.Textarea(attrs={
								'class':'form-control',
								'placeholder':'pesan'
								}))