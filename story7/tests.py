from django.test import TestCase, Client, LiveServerTestCase
from django.urls import reverse, resolve
from .views import index, tambah
from .models import Pesan, Pesantemp
from .apps import Story7Config
from .forms import Formpesan
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options
#from .forms import Formkegiatan, Formpeserta
# Create your tests here. 
#get manggil html
#resolve func dipanggil
#
class storyTest(TestCase):

    def test_index_resolv(self):
        url = reverse('index')
        self.assertEquals(resolve(url).func,index)


    def test_index_exists(self):
        response = self.client.get(reverse('index'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,'index.html') 

    def test_model_1(self):
        model = Pesan.objects.create(nama='aaaa',pesan='bbbb',warna="blue",count=1)
        self.assertEqual(Pesan.objects.all().count(),1)
        self.assertEquals(str(Pesan.objects.get(pk=1)),'aaaa')

    def test_model_2(self):
        model = Pesantemp.objects.create(nama='aaaa',pesan='bbbb')
        self.assertEqual(Pesantemp.objects.all().count(),1)
        self.assertEquals(str(Pesantemp.objects.get(pk=1)),'aaaa')

    def test_tambah_exists(self):
        response = self.client.get('/tambah/')
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,'tambahpesan.html')

    def test_tambah_resolv(self):
        url = reverse('tambah')
        self.assertEquals(resolve(url).func,tambah)

    def test_apps(self):
        self.assertEqual(Story7Config.name, 'story7')

    def test_cektambah(self):
        form = Formpesan(data={'nama':"test",'pesan':"aaaa"})
        self.assertTrue(form.is_valid())
        response_post = self.client.post(reverse('cektambah'), {'nama':"test",'pesan':"aaaa"})
        self.assertEqual(response_post.status_code, 200)
        self.assertTemplateUsed(response_post, 'cekpesan.html')

    def test_validasi_ya(self):
        Pesantemp.objects.create(nama='aaaa',pesan='bbbb')
        response_post = self.client.post(reverse('validasi'), {'ya':"ya"})
        self.assertEqual(response_post.status_code, 302)

    def test_validasi_tidak(self):
        Pesantemp.objects.create(nama='aaaa',pesan='bbbb')
        response_post = self.client.post(reverse('validasi'), {'tidak':"tidak"})
        self.assertEqual(response_post.status_code, 302)

    def test_gantiwarna1(self):
        Pesan.objects.create(nama='aaaa',pesan='bbbb',warna="blue",count=1)
        response = self.client.get('/warna/1')
        self.assertEquals(response.status_code, 302)
        self.assertEquals(Pesan.objects.get(pk=1).warna, "green")

        response = self.client.get('/warna/1')
        self.assertEquals(Pesan.objects.get(pk=1).warna, "yellow")

        response = self.client.get('/warna/1')
        self.assertEquals(Pesan.objects.get(pk=1).warna, "red")

        response = self.client.get('/warna/1')
        self.assertEquals(Pesan.objects.get(pk=1).warna, "blue")    
    

class functionaltest(LiveServerTestCase):
    def setUp(self):
        super().setUp()

        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')


        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        
    def tearDown(self):
        self.browser.quit()

        super().tearDown()


    def test_tambah_gajadi_tapi_jadi(self):
        self.browser.get(self.live_server_url+"/")
        time.sleep(3)
        self.browser.find_element_by_name('tambah').send_keys(Keys.RETURN)
        time.sleep(3)

        self.browser.find_element_by_id('id_nama').send_keys("ini nama")
        self.browser.find_element_by_id('id_pesan').send_keys("ini pesan loo")
        time.sleep(3)
        self.browser.find_element_by_class_name('btn-success').send_keys(Keys.RETURN)
        time.sleep(2)

        self.browser.find_element_by_name('tidak').send_keys(Keys.RETURN)
        self.browser.find_element_by_id('id_nama').send_keys("ini nama lagi")
        self.browser.find_element_by_id('id_pesan').send_keys("ini pesan lagi loo")
        time.sleep(3)
        self.browser.find_element_by_class_name('btn-success').send_keys(Keys.RETURN)
        time.sleep(2)
        self.browser.find_element_by_name('ya').send_keys(Keys.RETURN)
        time.sleep(3)
        
if __name__=="__main__":
    unittest.main(warnings="ignore")


'''

    def test_tambahkegiatan(self):
        #ulr = self.client.post(data ='adsfadsf' )
        #self.assertEquals(resolve(url),)
        activity = Kegiatan.objects.create(nama='adfad')
        self.assertNotEqual(Kegiatan.objects.all().count(),0)
        self.assertEquals(str(Kegiatan.objects.get(pk=1)),'adfad')

    def test_tambahpeserta(self):
        #ulr = self.client.post(''data ='adsfadsf' )
        #self.assertEquals(resolve(url),)
        activity = Kegiatan.objects.create(nama='adfad')
        activity2 = Peserta.objects.create(nama='saya',kegiatan=activity)
        self.assertEquals(str(Peserta.objects.get(pk=1)),'saya')

    def test_story6_post(self):
        form = Formkegiatan(data={'nama':"test"})
        self.assertTrue(form.is_valid())
        response_post = self.client.post('/kegiatan/tk', {'nama':"dyhd"})
        self.assertEqual(response_post.status_code, 302)

        #response= Client().get('/story6/')
        #html_response = response.content.decode('utf8')
        #self.assertIn(test, html_response)
    def test_app(self):
        found = resolve('/kegiatan/tk')
        self.assertEqual(found.func, tambahkegiatan)
'''




