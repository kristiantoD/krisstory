from django.db import models

# Create your models here.
class Pesan(models.Model):
	nama = models.CharField(max_length=50)
	pesan = models.TextField()
	warna = models.CharField(max_length=50)
	count = models.IntegerField()
	
	def __str__(self):
		return self.nama

class Pesantemp(models.Model):
	nama = models.CharField(max_length=50)
	pesan = models.TextField()
	
	def __str__(self):
		return self.nama